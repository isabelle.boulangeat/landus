---
title: "analyse LANDES Mont-Blanc"
author: "Isabelle Boulangeat"
date: today
output:
  html_document:
    keep_md: yes
    variant: markdown_github
editor_options:
  chunk_output_type: console
always_allow_html: yes
---





```r
full_tab = read.csv("_data_prod/full_tab.csv")
```

## Distribution des proportions

Total landes species / total herbaceous species


```r
library(ggplot2)
ggplot(full_tab, aes(x = LH_proportion)) + geom_histogram()
```

```
## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
```

```
## Warning: Removed 2 rows containing non-finite values (`stat_bin()`).
```

![](analyse_step2_stats_files/figure-html/LH-1.png)<!-- -->

