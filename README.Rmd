---
title: "README"
author: "Isabelle Boulangeat"
date: "31/-1/2022"
output:
  pdf_document: default
  html_document:
    keep_md: yes
    variant: markdown_github
editor_options:
  chunk_output_type: console
always_allow_html: yes
---

```{r setup, include=FALSE}
library(knitr)
# library(kableExtra)
knitr::opts_chunk$set(echo = TRUE)
# library(Jmisc)
library(tidyr)

```

dat_phyto = readRDS("_data/cbna_phyto_chamonix.rds")

head(dat_phyto)
unique(dat_phyto$date_releve_deb) # 28 journees observations
length(unique(dat_phyto$id_releve)) # 246 releves

library(dplyr)
dat = dat_phyto %>% select(x_l93, y_l93, alti_calc, lib_exposition, pente, abondance, rec_total_pl, rec_herba_pl, rec_arbo_pl, rec_arbu_pl, rec_sarb_pl, rec_crypto_pl, haut_herba, haut_arbo, haut_arbu, haut_sarb, lib_syntaxon, nom_reconnu, id_releve, plage_pente)


## Analyse communautes vegetales
```{r ,fig=TRUE}
library(ggplot2)
ggplot(dat_long, aes(fill=stat, x=ref_typoveg, y=value)) +
  geom_bar(position = "dodge", stat = "identity") +
  facet_wrap(~stat, ncol = 1, scales = "free")

```
