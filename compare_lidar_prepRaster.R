plot_dem = read.csv("_data_prod/full_tab.csv")
head(plot_dem)

plot_dem$keyRaster = 1:nrow(plot_dem)
spg = plot_dem

library(raster)
ras_grid = raster("_data_prod/raster_grid.tif")
ras_grid
plot(ras_grid)
#--
rasSites = rasterize(plot_dem[,c("X93", "Y93")], ras_grid, plot_dem$keyRaster, res = c(25,25), digits = 1)
rasSites
plot(rasSites)

plot(aggregate(rasSites, 10))

# crop Raster* with Spatial* object
extentSmall <- as(extent(990000, 1020000, 6530000, 6560000), 'SpatialPolygons')
crs(extentSmall) <- crs(rasSites)
sitesCrop <- crop(rasSites, extentSmall)

plot(sitesCrop)
plot(aggregate(sitesCrop, 10))

writeRaster(sitesCrop, "_data_prod/raster_sites.tif", overwrite = TRUE)
#--

