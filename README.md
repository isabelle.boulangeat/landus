Enlever les sites CAMTRAP forêt (criteres <1900 et cameras)
tester les modèles joints https://github.com/TheoreticalEcology/s-jSDM
voir Jean Matthieu Monnet LIDAR
- resolution données hauteur
- traitement de la pente
- crossvalidation LIDAR <-> placettes mesurées

## Overview
Objectif:
- spatialisation des types de landes
	- proportions de LVU, LRF, LVVI, LCV + HF (et nb touches)
	- proportion L/H
	- autres stats de structure: mixité, .... total L, ...total Lxhauteur

facteurs à explorer
- hauteur  moyenne de la canopée
- couverture strate(s) >1m50 (dynamique de colonisation)
- distance à la forêt??
- pâturage -> effet site (ou delta orthophoto ou satellite)
- altitude
- heterogeneite de la hauteur
- sol nu (description site) + crossvalid OSO
- neige (longueur saison, +gdd sur la saison, date début saison)
- rayonnement du LERFOB (vérifier effets masque)
- pente
- topographie: distance à la crête, concavité?? ou twi

- [ ] Situer les cameras sur les gradients


## quelques descriptions en vrac
- [x] distribution des proportions de L / H -> à voir si cela distingue des communautés de landes
- [x]  distribution des proportions des PFG principaux
- [x]  distribution des hauteurs, nb PFG, ...
- [ ] heterogeneité vs PFG (notamment VM)
- [ ] graphs avec facteurs univariés et réponses
- [ ] relations 2 à 2 inter drivers
- [ ] proportion rhodo vs propotion HF ou Div Fct (nb de groupes) ::> c'est aussi un truc qu'on peut tester avec des releves phyto (ORCHAMP, CBNA), on peut aller jusqu'aux lien proportions de espèces de landes vs espèce d'intérêt
- [ ] rda/cca 
- [ ] replacer les valeurs sur des cartes (axes, ...)

## Données phytosocio
- [ ] presence espèces clé: voir liste espèces Chamois
- [ ] diversité spécifique

## plus tard (données brutes de points contact)
- estimer la taille des patchs -> stade de colonisation
- cooccurrences horizontales ou verticales