
################################################################################
# calc variables gdd
################################################################################

calc_vars_gdd_from_lsd <- function(tab_vars, lsd, end_obs, tbase=0, temp.col = "Tgdd", date.col = "ddate", farenheit = TRUE, snow = NULL, threshold.gdd = 5){
  
  LSD = lsd
  tab_vars = tab_vars[1:365,]
  snow = snow[1:365]

  defaut.output =  data.frame(cumgdd = 0,
                              nsnowdays_p30 = 30,
                              cumgdd_p30 = 0,
                              cumgdd30 = 0,
                              cumgdd60 = 0,
                              nsnowdays30 = 30, 
                              daygdd300 = NA,
                              daygdd600 = NA,
                              daygdd900 = NA,
                              speedgdd300 = NA,
                              speedgdd300600 = NA,
                              ndayfrost = NA,
                              ndayfrost300 = NA,
                              minTemp = NA, 
                              maxTemp =NA,
                              minTemp300 = NA, 
                              maxTemp300 = NA,
                              start_season = lsd, 
                              LSD = LSD, 
                              end_season = end_obs)
  
  if((lsd >= end_obs) | (lsd<=30 & end_obs<=32)) {
    output = defaut.output
  }else{
  
  if(farenheit) {
    temp_zero = 273.15
  }else temp_zero = 0
  
  require(lubridate)
  require(dplyr)
  tab_vars = as.data.frame(tab_vars)
  colnames(tab_vars)[which(colnames(tab_vars)==temp.col)] = "temp"
  colnames(tab_vars)[which(colnames(tab_vars)==date.col)] = "ddate"
  tab_vars$ddate = as.Date(tab_vars$ddate)
  
  ## correct LSD if too early
  if(lsd<=30) lsd = 31
  
  tab_vars = tab_vars %>% mutate(gdd = temp - temp_zero - tbase)  %>% mutate(gdd = ifelse(gdd<0, 0, gdd)) 
  if(!is.null(snow)){
    tab_vars = tab_vars %>% 
    mutate(gdd = ifelse(snow>0.05, 0, gdd))
    }
  tab_vars =tab_vars %>% mutate(cumgdd = cumsum(gdd))
  lsd.default=which(tab_vars$cumgdd>threshold.gdd)[1]
  lsd = max(lsd, lsd.default, na.rm=TRUE)
  
  ## end correction LSD
  
  if(end_obs-lsd<2){
    output = defaut.output
  }else {
    previous30 = tab_vars[(lsd-30):lsd, ]
    if(!is.null(snow)){
      previous30$sd = snow[(lsd-30):lsd]
      gdd_p30 = previous30 %>% mutate(gdd = temp - temp_zero - tbase)  %>% mutate(gdd = ifelse(gdd<0, 0, gdd)) %>% mutate(gdd = ifelse(sd>0.05, 0, gdd)) %>% mutate(cumgdd = cumsum(gdd))
      nsnowdays_p30 = length(which(previous30$sd >0.05))
      
      first30 = snow[lsd:(lsd+30)]
      nsnowdays30 = length(which(first30 >0.05))
      
      gdd_tab = tab_vars[lsd:nrow(tab_vars),]
      gdd_tab$sd = snow[lsd:nrow(tab_vars)]
      gdd_tab = gdd_tab %>% mutate(gdd = temp - temp_zero - tbase)  %>%
        mutate(gdd = ifelse(gdd<0, 0, gdd))  %>% 
        mutate(gdd = ifelse(sd>0.05, 0, gdd))%>%
        mutate(cumgdd = cumsum(gdd))
      
    }else{
      gdd_p30 = previous30 %>% mutate(gdd = temp - temp_zero - tbase)  %>%
        mutate(gdd = ifelse(gdd<0, 0, gdd)) %>% mutate(cumgdd = cumsum(gdd)) 
      nsnowdays30 = nsnowdays30_p30 = NA
      gdd_tab = tab_vars[lsd:nrow(tab_vars),] %>% mutate(gdd = temp - temp_zero - tbase)  %>%
        mutate(gdd = ifelse(gdd<0, 0, gdd))  %>% 
        mutate(cumgdd = cumsum(gdd))
      
    }
    
    gdd300 = lubridate::yday(first(gdd_tab[which(gdd_tab$cumgdd>300),"ddate"]))
    gdd600 = lubridate::yday(first(gdd_tab[which(gdd_tab$cumgdd>600),"ddate"]))
    gdd900 = lubridate::yday(first(gdd_tab[which(gdd_tab$cumgdd>900),"ddate"]))
    index300 = first(which(gdd_tab$cumgdd>300))
    if(is.na(index300)) {
      ndf300 = NA
      mst300 = NA
      maxt300 = NA
    }else{
      ndf300 = length(which((gdd_tab[1:index300 ,"temp"]-temp_zero)<0))
      mst300 = min(gdd_tab[1:index300 ,"temp"]-temp_zero)
      maxt300 = max(gdd_tab[1:index300, "temp"]-temp_zero)
    }
    
    
    temp_series = gdd_tab$temp[1:(end_obs-lsd)]-temp_zero
    
    ## output
    output = data.frame(cumgdd = gdd_tab$cumgdd[end_obs-lsd], # end_obs index = end_obs - lsd dans gdd_tab
                        nsnowdays_p30 = nsnowdays_p30,
                        cumgdd_p30 = gdd_p30$gdd[length(gdd_p30)],
                        cumgdd30 = gdd_tab$cumgdd[30],
                        cumgdd60 = gdd_tab$cumgdd[60],
                        nsnowdays30 = nsnowdays30, 
                        daygdd300 = gdd300,
                        daygdd600 = gdd600,
                        daygdd900 = gdd900,
                        speedgdd300 = gdd300-lsd,
                        speedgdd300600 = gdd600-gdd300,
                        ndayfrost = length(which(temp_series<0)),
                        ndayfrost300 = ndf300,
                        minTemp = min(temp_series), 
                        maxTemp = max(temp_series),
                        minTemp300 = mst300, 
                        maxTemp300 = maxt300,
                        start_season = lsd, 
                        LSD = LSD, 
                        end_season = end_obs
    )
  }
  }
  
  
  return(output)
}



#==================================================================
# wrapper extract variables 
#==================================================================
crosscut_calc_gdd_periods <- function(path_csv_snow= "_data_raw/", y, tbase = 0, snow.col = "sd", temp.col = "soil_temp5mm",  date.col = "ddate", farenheit=TRUE, snow_threshold = 0.05, slide_window = 5, threshold.gdd = 5, lseason.min = 2){
  #ßprint(y)
  require(lubridate)
  require(dplyr)

  data_sveg = crosscut_extract_data (path_csv_snow, y)
  data_sveg = as.data.frame(data_sveg)
  colnames(data_sveg)[which(colnames(data_sveg)==snow.col)] = "snow_depth"
  colnames(data_sveg)[which(colnames(data_sveg)==temp.col)] = "temp"
  colnames(data_sveg)[which(colnames(data_sveg)==date.col)] = "ddate"
  data_sveg$ddate = as.Date(data_sveg$ddate)
  data_sveg$year = year(data_sveg$ddate)

  
  
  # identify seasons > lseason.min jours
  locs =  data_sveg %>%  dplyr::select(loc) %>% pull() %>% unique()
  sdata = data_sveg %>% group_by(loc) %>% group_by(loc) %>% group_map( ~ identify_sveg(.x$snow_depth, snow_threshold = snow_threshold, slide_window = slide_window))
  season = data.frame(loc = locs , lseason= unlist(lapply(sdata, length)))
  interval = lapply(sdata[which(season$lseason>lseason.min)], function(x){data.frame(lsd = x[1], fsd = x[length(x)])}) %>% bind_rows()
  interval$loc = season[which(season$lseason>lseason.min),"loc"]
  season_params = interval %>% left_join(season)
  
  wrap_calc_vars_gdd_from_lsd = function(tab){
    #print(tab$loc[1])
    calc_vars_gdd_from_lsd(tab, lsd = tab$lsd[1], end_obs = tab$fsd[1], temp.col = temp.col,  date.col = date.col,snow = tab$snow_depth, threshold.gdd = threshold.gdd)
  }
  # calc variables
  output = data_sveg %>% left_join(season_params) %>% na.omit()  %>% group_by(loc) %>% group_map( ~ wrap_calc_vars_gdd_from_lsd(.x), .keep=TRUE ) %>% bind_rows()
  output$loc = data_sveg %>% left_join(season_params) %>% na.omit() %>% dplyr::select(loc) %>% pull() %>% unique()

  return(output)
  
}

#test
# crosscut_calc_gdd_periods("_data_raw/", 2016)

################################################################################
#   wrapper for gdd from air temp (safran)
################################################################################

safran_calc_gdd_periods <- function(path_data_allslopes, y, tab_loc_nop, safran.colname = "nop",crocus.colname = "loc", tbase = 0, snow_threshold = 0.05, slide_window = 5, threshold.gdd = 5, path_data_snow){
  
  colnames(tab_loc_nop)[which(colnames(tab_loc_nop)==safran.colname)] = "nop"
  colnames(tab_loc_nop)[which(colnames(tab_loc_nop)==crocus.colname)] = "loc"
  
  
  print(y)
  require(dplyr)
  require(abind)
  
  # load safran temp and precip
  data = extract_season_vars_meteo(path_data_allslopes, y, unique(tab_loc_nop$nop))
  print("data extracted from ncdf")
  data_temp = calc_temperatures (data$dat_meteo, data$dates_meteo)
  
  data_precip = calc_precip (data$dat_meteo, data$dates_meteo) %>% bind_rows(.id = "nop")  %>% dplyr::select(nop, days, PP)
  
  data_sveg = data_temp %>% bind_rows(.id = "nop") %>% full_join(data_precip, by = c("nop", "days"))
  data_sveg$ddate = as.Date(data_sveg$days)
  data_sveg$year = lubridate::year(data_sveg$ddate)
  data_sveg = data_sveg %>% filter( year == y)
  
  ## load snow
  data_crocus = crosscut_extract_data (path_data_snow, y)
 
  result= lapply(1:nrow(tab_loc_nop), function(i){
    no = tab_loc_nop[i, "nop"]
    lo =tab_loc_nop[i, "loc"]
    dnop = data_sveg %>% filter (nop == no)
    snow_vec_loc =  data_crocus %>% filter (loc == lo)
    season = identify_sveg(snow_vec_loc$sd)
    
    tabres = calc_vars_gdd_from_lsd(dnop, lsd = season[1], end_obs = season[length(season)], tbase=0, temp.col = "Tgdd", date.col = "ddate", farenheit = FALSE, snow = snow_vec_loc$sd, threshold.gdd = threshold.gdd)
    
    if(!is.na(tabres$start_season)){
      tabres$cumprecip = sum(dnop[tabres$start_season:tabres$end_season,"PP"])
      if(!is.na(tabres$daygdd300))
      {
        tabres$precip300 = sum(dnop[tabres$start_season:tabres$daygdd300,"PP"])
        tabres$ndayfrost300_Tmin_min = sum(dnop[tabres$start_season:tabres$daygdd300,"Tmin"]<0)
        tabres$minTemp300_Tmin_min = min(dnop[tabres$start_season:tabres$daygdd300,"Tmin"])
        tabres$maxTemp300_Tmax_max = max(dnop[tabres$start_season:tabres$daygdd300,"Tmax"])
        
      }
      
      if(tabres$start_season>30){
        tabres$cumprecip_p30 = sum(dnop[(tabres$start_season-30):tabres$start_season,"PP"])
      }
      tabres$sumprecip30 = sum(dnop[tabres$start_season:(tabres$start_season+30),"PP"])
      
    }else{
      tabres$sumprecip30 =  tabres$sumprecip_p30 =  tabres$cumprecip = tabres$precip300 = tabres$ndayfrost300_Tmin_min = tabres$minTemp300_Tmin_min = tabres$maxTemp300_Tmax_max = NA
    }   
   
    return(tabres)
  }) %>% bind_rows()
  
  output = bind_cols(tab_loc_nop, result)
  
  colnames(tab_loc_nop)[which(colnames(tab_loc_nop)=="nop")] = "safran.loc"
  colnames(tab_loc_nop)[which(colnames(tab_loc_nop)=="loc")] = "crocus.loc"
  
  return(output)
  
  
  
}

# safran_calc_gdd_periods(path_data_allslopes, 2017, tab, nop.colname = "simuls_nop", farenheit = FALSE)


# SPECIFIC DATE
################################################################################

safran_calc_date_vars <- function(path_data_allslopes, y,  tab_date_nop, safran.colname = "nop",crocus.colname = "loc",date.colname = "date_releve", tbase = 0, snow_threshold = 0.05, slide_window = 5, threshold.gdd = 5, path_data_snow){
  
  tab_date_nop = tab_date_nop %>% filter(year == y)
  colnames(tab_date_nop)[which(colnames(tab_date_nop)==safran.colname)] = "nop"
  colnames(tab_date_nop)[which(colnames(tab_date_nop)==crocus.colname)] = "loc"
  colnames(tab_date_nop)[which(colnames(tab_date_nop)==date.colname)] = "date_releve"
  
  print(y)
  require(dplyr)
  require(abind)
  
  require(lubridate)
  data = extract_season_vars_meteo(path_data_allslopes, y, unique(tab_date_nop$nop))
  print("data extracted from ncdf")
  data_temp = calc_temperatures (data$dat_meteo, data$dates_meteo)
  data_precip = calc_precip (data$dat_meteo, data$dates_meteo) %>% bind_rows(.id = "nop")  %>% dplyr::select(nop, days, PP)
  
  data_sveg = data_temp %>% bind_rows(.id = "nop") %>% full_join(data_precip, by = c("nop", "days"))
  data_sveg$ddate = as.Date(data_sveg$days)
  data_sveg$year = lubridate::year(data_sveg$ddate)
  data_sveg = data_sveg %>% filter( year == y)

  ## load snow
  data_crocus = crosscut_extract_data (path_data_snow, y)
  tab_date_nop$day_releve = lubridate::yday(tab_date_nop$date_releve)
  
  result= lapply(1:nrow(tab_date_nop), function(i){
    no = tab_date_nop[i, "nop"]
    lo =tab_date_nop[i, "loc"]
    dnop = data_sveg %>% filter (nop == no)
    snow_vec_loc =  data_crocus %>% filter (loc == lo)
    season = identify_sveg(snow_vec_loc$sd)
    date_releve = tab_date_nop[i, "day_releve"]
    
    tabres = calc_vars_gdd_from_lsd(dnop, lsd = season[1], end_obs = date_releve, tbase=0, temp.col = "Tgdd", date.col = "ddate", farenheit = FALSE, snow = snow_vec_loc$sd, threshold.gdd = threshold.gdd)
    
    if(!is.na(tabres$start_season)){
      tabres$cumprecip = sum(dnop[tabres$start_season:tabres$end_season,"PP"])
      if(!is.na(tabres$daygdd300))
      {
        tabres$precip300 = sum(dnop[tabres$start_season:tabres$daygdd300,"PP"])
        tabres$ndayfrost300_Tmin_min = sum(dnop[tabres$start_season:tabres$daygdd300,"Tmin"]<0)
        tabres$minTemp300_Tmin_min = min(dnop[tabres$start_season:tabres$daygdd300,"Tmin"])
        tabres$maxTemp300_Tmax_max = max(dnop[tabres$start_season:tabres$daygdd300,"Tmax"])
        
      }
      
      if(tabres$start_season>30){
        tabres$cumprecip_p30 = sum(dnop[(tabres$start_season-30):tabres$start_season,"PP"])
      }
      tabres$sumprecip30 = sum(dnop[tabres$start_season:(tabres$start_season+30),"PP"])
      
    }else{
      tabres$sumprecip30 =  tabres$sumprecip_p30 =  tabres$cumprecip = tabres$precip300 = tabres$ndayfrost300_Tmin_min = tabres$minTemp300_Tmin_min = tabres$maxTemp300_Tmax_max = NA
    }   
    
    return(tabres)
  }) %>% bind_rows()
  
  output = bind_cols(tab_date_nop, result)
  
  colnames(tab_date_nop)[which(colnames(tab_date_nop)=="nop")] = "safran.loc"
  colnames(tab_date_nop)[which(colnames(tab_date_nop)=="loc")] = "crocus.loc"
  
  return(output)
  
  
  
  return(tab_date_nop)
  
}


